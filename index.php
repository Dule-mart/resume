<!DOCTYPE html>
<html lang="en" class="no-js">
  <head>
    <meta charset="UTF-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge"> 
    <title>Tabah Baridule Martins - Resume</title>
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1" />
    <meta name="description" content="Dule Martins - Resume" />
    <meta name="keywords" content="vcard, resposnive, retina, resume, jquery, css3, bootstrap, Material CV, developer, 100daysofcode, portfolio" />
    <meta name="author" content="lmtheme" />
    <link rel="shortcut icon" href="icon.png">

    <link rel="stylesheet" href="css/bootstrap.min.css" type="text/css">
    <link rel="stylesheet" href="css/normalize.css" type="text/css">
    <link rel="stylesheet" href="css/animate.css" type="text/css">
    <link rel="stylesheet" href="css/transition-animations.css" type="text/css">
    <link rel="stylesheet" href="css/owl.carousel.css" type="text/css">
    <link rel="stylesheet" href="css/magnific-popup.css" type="text/css">
    <link rel="stylesheet" href="css/main.css" type="text/css">
  </head>

  <body class="material-template">
    <!-- Loading animation -->
    <div class="preloader">
      <div class="preloader-animation">
        <div class="preloader-spinner">
        </div>
      </div>
    </div>
    <!-- /Loading animation -->

    <div id="page" class="page">
      <!-- Header -->
      <header id="site_header" class="header mobile-menu-hide">
        <div class="header-content">
          <div class="site-title-block mobile-hidden">
            <div class="site-title">Tabah <span>Baridule Martins</span></div>
          </div>

          <!-- Navigation -->
          <div class="site-nav">
            <!-- Main menu -->
            <ul id="nav" class="site-main-menu">
              <li>
                <a class="pt-trigger" href="#home" data-animation="62">Home</a><!-- href value = data-id without # of .pt-page. -->
              </li>
              <li>
                <a class="pt-trigger" href="#resume" data-animation="62">Resume</a>
              </li>
              <li>
                <a class="pt-trigger" href="#services" data-animation="62">Services</a>
              </li>
              <li>
                <a class="pt-trigger" href="#portfolio" data-animation="62">Portfolio</a>
              </li>
              <li>
                <a class="pt-trigger" href="#contact" data-animation="62">Contact</a>
              </li>
            </ul>
            <!-- /Main menu -->
          </div>
          <!-- Navigation -->
        </div>
      </header>
      <!-- /Header -->

      <!-- Mobile Header -->
      <div class="mobile-header mobile-visible">
        <div class="mobile-logo-container">
          <div class="mobile-site-title">Wisdom Matthew</div>
        </div>

        <a class="menu-toggle mobile-visible">
          <i class="fa fa-bars"></i>
        </a>
      </div>
      <!-- /Mobile Header -->

      <!-- Main Content -->
      <div id="main" class="site-main">
        <!-- Page changer wrapper -->
        <div class="pt-wrapper">
          <!-- Subpages -->
          <div class="subpages">

            <!-- Home Subpage -->
            <section class="pt-page" data-id="home">
              <div class="section-inner start-page-content">
                <div class="page-header">
                  <div class="row">
                    <div class="col-sm-4 col-md-4 col-lg-4">
                      <div class="photo">
                        <img src="images/photo.jpg" alt="">
                      </div>
                    </div>

                    <div class="col-sm-8 col-md-8 col-lg-8">
                      <div class="title-block">
                        <h1>Wisdom Matthew</h1>
                        <div class="owl-carousel text-rotation">                                    
                          <div class="item">
                            <div class="sp-subtitle">Web Developer</div>
                          </div>
                          <div class="item">
                            <div class="sp-subtitle">Backend Web developer</div>
                          </div>
                          <div class="item">
                            <div class="sp-subtitle">Programming Instructor</div>
                          </div>
                        </div>
                      </div>

                      <div class="social-links">
                        <a href="https://github.com/wisdommatt"><i class="fa fa-github"></i></a>
                        <a href="https://twitter.com/wisdommatthew11"><i class="fa fa-twitter"></i></a>
                        <a href="https://web.facebook.com/wisdomchinonso.matthew"><i class="fa fa-facebook"></i></a>
                        <a href="https://www.instagram.com/wizdommatt/"><i class="fa fa-instagram"></i></a>
                      </div>
                    </div>
                  </div>
                </div>

                <div class="page-content">
                  <div class="row">

                    <div class="col-sm-6 col-md-6 col-lg-6">
                      <div class="about-me">
                        <div class="block-title">
                          <h3>About <span>Me</span></h3>
                        </div>
                        <p>Hello! I’m Wisdom Matthew. A web developer. I’ve worked internationally, in-house, and remotely on projects for leading brands, agencies, startups, and charities. I care deeply about creating world-class, useful, and beautiful products that help people and make a difference. I can be as involved in your project as you need me to be; from the seed of the idea, to development.</p>
                      </div>
<!--                       <div class="download-resume">
                        <a href="" class="btn btn-secondary">Download Resume</a>
                      </div> -->
                    </div>

                    <div class="col-sm-6 col-md-6 col-lg-6">
                      <ul class="info-list">
                        <li><span class="title">Age</span><span class="value">18</span></li>
                        <li><span class="title">Residence</span><span class="value">NIGERIA</span></li>
                        <li><span class="title">Address</span><span class="value">110 East West Road, Rumuodara
                        Port Harcourt, Nigeria</span></li>
                        <li><span class="title">e-mail</span><span class="value"><a href="mailto:talk2wisdommatt@gmail.com">talk2wisdommatt@gmail.com</a></span></li>
                        <li><span class="title">Phone</span><span class="value">+234 - 706 - 763 - 3750</span></li>
                        <li><span class="title">Freelance</span><span class="value available">Available</span></li>
                      </ul>
                    </div>

                  </div>
                </div>
              </div>
            </section>
            <!-- End of Home Subpage -->

            <!-- Resume Subpage -->
            <section class="pt-page" data-id="resume">
              <div class="section-inner custom-page-content">
                <div class="page-header color-1">
                  <h2>Resume</h2>
                </div>
                <div class="page-content">

                  
                  <div class="row">
                    <div class="col-sm-6 col-md-6 col-lg-6">
                      <div class="block">
                        <div class="block-title">
                          <h3>Education</h3>
                        </div>

                        <div class="timeline">
                          <!-- Education 1 -->
                          <div class="timeline-item">
                            <h4 class="item-title">Junior Secondary School</h4>
                            <span class="item-period">2011 - 2014</span>
                            <span class="item-small">Wisdom Gate School</span>
                            <p class="item-description">Junior Secondary School @ Wisdom Gate</p>
                          </div>
                          <!-- / Education 1 -->

                          <!-- Education 2 -->
                          <div class="timeline-item">
                            <h4 class="item-title">Senior Secondary School</h4>
                            <span class="item-period">2014 - 2017</span>
                            <span class="item-small">Wisdom Gate School</span>
                            <p class="item-description">Senior Secondary School @ Wisdom Gate</p>
                          </div>
                          <!-- / Education 2 -->

                          <!-- Education 3 -->
                          <div class="timeline-item">
                            <h4 class="item-title">Bachelor's Degree in Computer Science</h4>
                            <span class="item-period">2019 - Till Date</span>
                            <span class="item-small">Rivers State University</span>
                            <p class="item-description">Bsc in Computer Science @ Rivers State University</p>
                          </div>
                          <!-- / Education 3 -->
                        </div>
                      </div>
                    </div>

                    <div class="col-sm-6 col-md-6 col-lg-6">
                      <div class="block">
                        <div class="block-title">
                          <h3>Experience</h3>
                        </div>

                        <div class="timeline">

                          <div class="timeline-item">
                            <h4 class="item-title">Web developer nanodegree</h4>
                            <span class="item-period">Jan 2017 - May 2017</span>
                            <span class="item-small">Udacity.com</span>
                            <p class="item-description">Web developer nanodegree @ Udacity.com</p>
                          </div>

                          <div class="timeline-item">
                            <h4 class="item-title">Web developer bootcamp</h4>
                            <span class="item-period">June 2017 - Oct 2017</span>
                            <span class="item-small">Udemy.com</span>
                            <p class="item-description">Web developer bootcamp @ Udemy.com</p>
                          </div>

                          <!-- Experience 1 -->
                          <div class="timeline-item">
                            <h4 class="item-title">Backend-developer intern</h4>
                            <span class="item-period">Sep 2018 - Jan 2019</span>
                            <span class="item-small">Primed Soft</span>
                            <p class="item-description">Backend-developer intern @ Primed soft</p>
                          </div>
                          <!-- / Experience 1 -->

                          <!-- Experience 2 -->
                          <div class="timeline-item">
                            <h4 class="item-title">Backend-Developer</h4>
                            <span class="item-period">Jan 2019 - Till Date</span>
                            <span class="item-small">Meghee Inc.</span>
                            <p class="item-description">Backend developer @ <a href="https://meghee.com" target="_blank">Meghee</a></p>
                          </div>
                          <!-- / Experience 2 -->
                        </div>
                      </div>
                    </div>
                  </div>

                  <div class="row">
                    <div class="col-sm-6 col-md-6 col-lg-6">
                      <div class="block">
                        <div class="block-title">
                          <h3>Web Development <span>Skills</span></h3>
                        </div>

                        <div class="skills-info">
                          <h4>Backend</h4>                               
                          <div class="skill-container">
                            <div class="skill-percentage skill-4"></div>
                          </div>

                          <h4>Frontend</h4>
                          <div class="skill-container">
                            <div class="skill-percentage skill-1"></div>
                          </div>

                          <h4>Fullstack</h4>
                          <div class="skill-container">
                            <div class="skill-percentage skill-4"></div>
                          </div> 
                        </div>

                      </div>
                    </div>

                    <div class="col-sm-6 col-md-6 col-lg-6">
                      <div class="block">
                        <div class="block-title">
                          <h3>Programming <span>Skills</span></h3>
                        </div>

                        <div class="skills-info">
                          <h4>HTML5</h4>                               
                          <div class="skill-container">
                            <div class="skill-percentage skill-4"></div>
                          </div>

                          <h4>CSS3</h4>
                          <div class="skill-container">
                            <div class="skill-percentage skill-5"></div>
                          </div>

                          <h4>Bootstrap</h4>
                          <div class="skill-container">
                            <div class="skill-percentage skill-3"></div>
                          </div>

                          <h4>Javascript</h4>
                          <div class="skill-container">
                            <div class="skill-percentage skill-2"></div>
                          </div>  

                          <h4>jQuery</h4>
                          <div class="skill-container">
                            <div class="skill-percentage skill-3"></div>
                          </div> 

                          <h4>PHP</h4>
                          <div class="skill-container">
                            <div class="skill-percentage skill-4"></div>
                          </div>

                          <h4>Python</h4>
                          <div class="skill-container">
                            <div class="skill-percentage skill-2"></div>
                          </div> 

                          <h4>Node.js</h4>
                          <div class="skill-container">
                            <div class="skill-percentage skill-2"></div>
                          </div> 

                          <h4>SQL {MySQL, SQLite, Oracle}</h4>
                          <div class="skill-container">
                            <div class="skill-percentage skill-4"></div>
                          </div> 

                          <h4>MongoDB</h4>
                          <div class="skill-container">
                            <div class="skill-percentage skill-3"></div>
                          </div> 

                          <h4>Redis</h4>
                          <div class="skill-container">
                            <div class="skill-percentage skill-3"></div>
                          </div> 

                        </div>
                        
                      </div>
                    </div>
                  </div>

                  <!-- Download Resume Button -->
                 <!--  <div class="row">
                    <div class="col-sm-12 col-md-12 col-lg-12">
                      <div class="block">
                        <div class="center download-resume">
                          <a href="#" class="btn btn-secondary">Download Resume</a>
                        </div>
                      </div>
                    </div>
                  </div> -->
                  <!-- End of Download Resume Button -->

                </div>
              </div>
            </section>
            <!-- End of Resume Subpage -->


            <!-- Services Subpage -->
            <section class="pt-page" data-id="services">
              <div class="section-inner custom-page-content">
                <div class="page-header color-1">
                  <h2>Services</h2>
                </div>
                <div class="page-content">
                  <!-- My Services -->
                  <div class="row">
                    <div class="col-sm-12 col-md-12">
                      <div class="block-title">
                        <h3>My <span>Services</span></h3>
                      </div>
                    </div>

                    <div class="col-sm-6 col-md-4">
                      <div class="service-block"> 
                        <div class="service-info">
                          <!--<i class="service-icon fa fa-shopping-cart"></i>-->
                          <div class="service-image">
                            <img src="images/service/web_design_icon.png" alt="Backend" class="mCS_img_loaded">
                          </div>
                          <h4>Backend Web Development</h4>
                          <p>I can help you or your organization handle all your backend / server side developments.</p>
                        </div>
                      </div>
                    </div>

                    <div class="col-sm-6 col-md-4">
                      <div class="service-block"> 
                        <div class="service-info">
                          <div class="service-image">
                            <img src="images/service/creativity_icon.png" alt="Creativity" class="mCS_img_loaded">
                          </div>
                          <h4>Frontend Web Development</h4>
                          <p>I can handle you or your organization frontend developments and deployments.</p>
                        </div>
                      </div>
                    </div>

                    <div class="col-sm-6 col-md-4">
                      <div class="service-block"> 
                        <div class="service-info">
                          <div class="service-image">
                            <img src="images/service/fullstack.png" alt="Advetising" class="mCS_img_loaded">
                          </div>
                          <h4>Fullstack Web Development</h4>
                          <p>I can build or develop a full web application for you or your organization.</p>
                        </div>
                      </div>
                    </div>
                  </div>
                  <!-- End of My Services -->

                </div>
              </div>
            </section>
            <!-- End of Services Subpage -->


            <!-- Portfolio Subpage -->
            <section class="pt-page" data-id="portfolio">
              <div class="section-inner custom-page-content">
                <div class="page-header color-1">
                  <h2>Portfolio</h2>
                </div>
                <div class="page-content">

                  <!-- Portfolio Content -->
                  <div class="portfolio-content">
                                
                    <!-- Portfolio filter -->
                    <!-- <ul id="portfolio_filters" class="portfolio-filters">
                      <li class="active">
                        <a class="filter btn btn-sm btn-link active" data-group="all">All</a>
                      </li>
                      <li>
                        <a class="filter btn btn-sm btn-link" data-group="media">Media</a>
                      </li>
                      <li>
                        <a class="filter btn btn-sm btn-link" data-group="illustration">Illustration</a>
                      </li>
                      <li>
                        <a class="filter btn btn-sm btn-link" data-group="video">Video</a>
                      </li>
                    </ul> -->
                    <!-- End of Portfolio filter -->

                    <!-- Portfolio Grid -->
                    <div id="portfolio_grid" class="portfolio-grid portfolio-masonry masonry-grid-3">

                      <!-- Portfolio Item 1 -->
                      <figure class="item" data-groups='["all", "media"]'>
                        <a class="ajax-page-load" href="https://timiun.com">
                          <img src="images/portfolio/timiun.jpg" alt="">
                          <div>
                            <h5 class="name">Timiun</h5>
                            <small>Ecommerce</small>
                            <!-- <i class="fa fa-file-text-o"></i> -->
                          </div>
                        </a>
                      </figure>

                      <figure class="item" data-groups='["all", "media"]'>
                        <a class="ajax-page-load" href="https://cloudnotte.com">
                          <img src="images/portfolio/cloudnotte.png" alt="">
                          <div>
                            <h5 class="name">Cloudnotte</h5>
                            <small>School Automation System</small>
                            <!-- <i class="fa fa-file-text-o"></i> -->
                          </div>
                        </a>
                      </figure>

                      <figure class="item" data-groups='["all", "media"]'>
                        <a class="ajax-page-load" href="https://dolfnet.org">
                          <img src="images/portfolio/dolfnet.jpg" alt="">
                          <div>
                            <h5 class="name">Dolfnet</h5>
                            <small>NGO - Client Aid in retention & treatment system</small>
                            <!-- <i class="fa fa-file-text-o"></i> -->
                          </div>
                        </a>
                      </figure>

                      <!-- /Portfolio Item 1 -->

                    </div>
                    <!-- /Portfolio Grid -->

                  </div>
                  <!-- /Portfolio Content -->
                </div>
              </div>
            </section>
            <!-- /Portfolio Subpage -->


            <!-- Contact Subpage -->
            <section class="pt-page" data-id="contact">
              <div class="section-inner custom-page-content">
                <div class="page-header color-1">
                  <h2>Contact</h2>
                </div>
                <div class="page-content">

                  <div class="row">
                    <div class="col-sm-6 col-md-6">
                      <div class="block-title">
                        <h3>Get in <span>Touch</span></h3>
                      </div>
                      <!-- Google Map -->
                      <!-- <div id="map" class="map"></div> -->
                      <!-- End of Google Map -->

                      <div class="contact-info-block">
                        <div class="ci-icon">
                          <i class="fa fa-map-marker"></i>
                        </div>
                        <div class="ci-text">
                          <h5>110 East West Road, Rumuodara
                        Port Harcourt, Nigeria</h5>
                        </div>
                      </div>
                      <div class="contact-info-block">
                        <div class="ci-icon">
                          <i class="fa fa-envelope"></i>
                        </div>
                        <div class="ci-text">
                          <h5>talk2wisdommatt@gmail.com</h5>
                        </div>
                      </div>
                      <div class="contact-info-block">
                        <div class="ci-icon">
                          <i class="fa fa-phone"></i>
                        </div>
                        <div class="ci-text">
                          <h5>+234 - 706 - 763 - 3750</h5>
                        </div>
                      </div>
                      <div class="contact-info-block">
                        <div class="ci-icon">
                          <i class="fa fa-check"></i>
                        </div>
                        <div class="ci-text">
                          <h5>Freelance Available</h5>
                        </div>
                      </div>
                    </div>

                    <!-- <div class="col-sm-6 col-md-6">
                      <div class="block-title">
                        <h3>Contact <span>Form</span></h3>
                      </div>
                      <form id="contact-form" method="post" action="contact_form/contact_form.php">

                        <div class="messages"></div>

                        <div class="controls">
                          <div class="form-group form-group-with-icon">
                            <i class="fa fa-user"></i>
                            <label>Full Name</label>
                            <input id="form_name" type="text" name="name" class="form-control" placeholder required="required" data-error="Name is required.">
                            <div class="form-control-border"></div>
                            <div class="help-block with-errors"></div>
                          </div>

                          <div class="form-group form-group-with-icon">
                            <i class="fa fa-envelope"></i>
                            <label>Email Address</label>
                            <input id="form_email" type="email" name="email" class="form-control" placeholder required="required" data-error="Valid email is required.">
                            <div class="form-control-border"></div>
                            <div class="help-block with-errors"></div>
                          </div>

                          <div class="form-group form-group-with-icon">
                            <i class="fa fa-comment"></i>
                            <label>Message for Me</label>
                            <textarea id="form_message" name="message" class="form-control" placeholder rows="4" required="required" data-error="Please, leave me a message."></textarea>
                            <div class="form-control-border"></div>
                            <div class="help-block with-errors"></div>
                          </div>

                          <div class="g-recaptcha" data-sitekey="6LdqmCAUAAAAAMMNEZvn6g4W5e0or2sZmAVpxVqI"></div>

                          <input type="submit" class="button btn-send" value="Send message">
                        </div>
                      </form>
                    </div> -->
                  </div>
                </div>
              </div>
            </section>
            <!-- End Contact Subpage -->

          </div>
        </div>
        <!-- /Page changer wrapper -->
      </div>
      <!-- /Main Content -->
    </div>
    <footer>
      <div class="copyrights">© <?= date('Y') ?> Meghee</div>
    </footer>

    <script src="js/jquery-2.1.3.min.js"></script>
    <script src="js/modernizr.custom.js"></script>

    <script type="text/javascript" src="js/bootstrap.min.js"></script>
    <script type="text/javascript" src="js/pages-switcher.js"></script>
    <script type="text/javascript" src="js/imagesloaded.pkgd.min.js"></script>
    <script type="text/javascript" src="js/validator.js"></script>
    <script type="text/javascript" src="js/jquery.shuffle.min.js"></script>
    <script type="text/javascript" src="js/masonry.pkgd.min.js"></script>
    <script type="text/javascript" src="js/owl.carousel.min.js"></script>
    <script type="text/javascript" src="js/jquery.magnific-popup.min.js"></script>
    <script type="text/javascript" src="js/jquery.hoverdir.js"></script>
    <!--<script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key=YOUR-API-KEY"></script>-->
    <script type="text/javascript" src="https://maps.googleapis.com/maps/api/js"></script>
    <!-- <script type="text/javascript" src="js/jquery.googlemap.js"></script> -->
    <script type="text/javascript" src="js/main.js"></script>
  </body>
</html>
